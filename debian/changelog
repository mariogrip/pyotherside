pyotherside (1.5.9-2) unstable; urgency=medium

  [ Felix Zielcke ]
  * Use debhelper compat level 13.
  * Remove usage of dh-exec. Not needed anymore.
  * Bump Standards-Version to 4.5.0.
  * Install qtquicktests and associated files inside pyotherside-tests.
  * Update Maintainer field with new Python Team address.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Felix Zielcke <fzielcke@z-51.de>  Mon, 28 Sep 2020 08:34:21 +0200

pyotherside (1.5.9-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

  [ Felix Zielcke ]
  * New upstream version.
  * Set Rules-Requires-Root: no

 -- Felix Zielcke <fzielcke@z-51.de>  Sat, 15 Feb 2020 20:59:16 +0100

pyotherside (1.5.8-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove trailing whitespaces.

  [ Felix Zielcke ]
  * New upstream version.
  * Update to policy 4.4.0.
  * Update to debhelper compat level 12.

 -- Felix Zielcke <fzielcke@z-51.de>  Mon, 08 Jul 2019 19:18:04 +0200

pyotherside (1.5.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Felix Zielcke ]
  * Upload to unstable
  * New Maintainer. (Closes: #912720)
  * New upstream version 1.5.3.
    - Fixes compatibility with newer QT versions. (Closes: #880093)
  * Update watchfile. Patch by Jonah Brüchert.
  * d/control: Use https protocol in Homepage field.
  * d/control: Update to policy 4.2.1.
  * d/control: Add libqt5svg5-dev to the build dependencies.
  * Update to debhelper compat level 11 and use new debhelper-compat
    build dependency.
  * Move installed documentation to
    /usr/share/doc/qml-module-io-thp-pyotherside to follow
    recommendation of Policy §12.3.

 -- Felix Zielcke <fzielcke@z-51.de>  Sun, 25 Nov 2018 08:27:17 +0100

pyotherside (1.4.0-2) experimental; urgency=medium

  [ Zygmunt Krynicki ]
  * Rename the binary package to qml-module-io-thp-pyotherside, closes: #780708
  * Drop the (now obsolete) XS-Testsuite: autopkgtest header
  * Add Sylvain as an uploader

  [ Sylvain Pineau ]
  * debian/control: Multi-Arch: foreign for pyotherside-tests was wrong
    It was shipping an ELF file in /usr/lib/pyotherside/tests/tests
    pyotherside-tests is now Multi-Arch: same and the files are installed in
    /usr/lib/${DEB_HOST_MULTIARCH}
  * debian/control: Remove bd on python3 as we already build-depend on
    python3-dev
  * Fix autopkgtest to use the path containing DEB_HOST_MULTIARCH

  [ Dmitry Shachnev ]
  * Use dh-exec instead of an .install.in file to subst DEB_HOST_MULTIARCH

 -- Sylvain Pineau <sylvain.pineau@canonical.com>  Thu, 21 Jan 2016 21:35:07 +0100

pyotherside (1.4.0-1) experimental; urgency=medium

  [ Marco Trevisan (Treviño) ]
  * New upstream release
  * debian/control: Remove build dependency on qt5-default
  * debian/control: Updated Standards-Version to 3.9.6
  * debian/control: Adding myself as Uploader
  * debian/rules: Fixed some lintian errors

  [ Zygmunt Krynicki ]
  * debian/control: remove DPMT from uploaders (requested by Piotr Ożarowski)
  * debian/control: start short description with lower-case letter

 -- Zygmunt Krynicki <zygmunt.krynicki@canonical.com>  Thu, 19 Feb 2015 18:38:54 +0100

pyotherside (1.2.0-1) unstable; urgency=medium

  * Initial release. (Closes: #746294)

 -- Zygmunt Krynicki <zygmunt.krynicki@canonical.com>  Wed, 30 Apr 2014 15:58:00 -0400
